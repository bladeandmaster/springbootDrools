package com.pingan.core.entity;

public class Customer {
    String idNo;
    String name;

    public Customer(String idNo, String name) {
        this.idNo = idNo;
        this.name = name;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
