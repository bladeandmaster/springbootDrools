package com.pingan.core.service;

import com.pingan.core.dao.UserDao;
import com.pingan.core.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    /*
     *@desc
     *@param
     *@return 
     *@author qhb871
     *@date 2018/12/28 20:58
    */
    public List<UserInfo> selectAllUsers(){
        List<UserInfo> userInfoList = null;
        try{
            userInfoList = userDao.selectAllUsers();
        }catch (Exception e){}
        return userInfoList;
    }
}
