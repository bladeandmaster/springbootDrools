package com.pingan.core.service;

import org.kie.spring.KModuleBeanFactoryPostProcessor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.net.URL;

public class CustomerBeanFactoryPostProcessor extends KModuleBeanFactoryPostProcessor {
    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
        try {
            String path = "file:"+applicationContext.getResource("classpath:/").getURL().getPath();
            path = path.replace("classes/","").replace("test","production")+"resources/";
            this.kModuleRootUrl = new URL(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
