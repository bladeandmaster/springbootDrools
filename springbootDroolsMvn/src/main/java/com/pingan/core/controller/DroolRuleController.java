package com.pingan.core.controller;

import com.pingan.core.entity.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class DroolRuleController {

//    此方式需要在springboot启动类App里通过
//    @ImportResource("classpath:/spring-drools.xml")配置spring-drools.xml
//    并配置<bean id="kiePostProcessor" class="org.kie.spring.KModuleBeanFactoryPostProcessor" />
//    或者<bean id="kiePostProcessor" class="org.kie.spring.annotations.KModuleAnnotationPostProcessor" />
//    使用KModuleBeanFactoryPostProcessor只能使用@Autowired方式
//    使用KModuleAnnotationPostProcessor可以使用@Autowired和@KBase("kbase")两种方式
//
//    另外使用@KReleaseId(groupId = "com.pingan.core",artifactId = "drools",version = "LATEST")
//    需要在resources目录下新建META-INF/maven/pom.properties
//    并配置：
//        groupId = com.pingan.core
//        artifactId = drools
//        version = 1
//    不过谁这么蛋疼会使用这种方式呢，肯定还是使用@Autowired方式简单，如下：
//     @Autowired
//     private KieBase kieBase;
//
//    另外由于KModuleBeanFactoryPostProcessor找drools文件时,这里有个大前提是用maven构建。
//    非测试环境下，会在项目目录target/classes目录下找，而maven构建时，刚好resoucce下的资源文件是编译在classes
//    目录下的，所以能找到;
//    但是测试环境下，会在项目目录target/test-classe目录找，所以在pom.xml需要额外进行如下设置：
//     <testResource>
//           <directory>src/main/resources</directory>
//     </testResource>
//    这样maven构建时就会把src/main/resources目录下的资源文件也编译一份到target/test-classes目录下



    @Autowired
    //@KBase("kbase")
    //@KReleaseId(groupId = "com.pingan.core",artifactId = "drools",version = "LATEST")
    private KieBase kieBase;

    //http://localhost:8080/rule1
    @RequestMapping("/rule1")
    @ResponseBody
    public String rule1(){
        //StatefulKnowledgeSession
        KieSession kieSession = kieBase.newKieSession();

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("superbing");
        userInfo.setTelephone("13618607409");

        kieSession.insert(userInfo);
        //kieSession.setGlobal("log",log);
        int firedCount = kieSession.fireAllRules();
        //kieSession.dispose();
        System.out.println("触发了" + firedCount + "条规则");
        return "触发了" + firedCount + "条规则";
    }


    //这种方式需要在resource目录新建META-INF/kmoudule.xml文件
    @RequestMapping("/rule2")
    @ResponseBody
    public String rule2(){
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        KieSession kieSession = kContainer.newKieSession("ksession-rules");

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("superbing");
        userInfo.setTelephone("13618607409");

        kieSession.insert(userInfo);
        //kieSession.setGlobal("log",log);
        int firedCount = kieSession.fireAllRules();
        //kieSession.dispose();
        System.out.println("触发了" + firedCount + "条规则");
        return "触发了" + firedCount + "条规则";
    }
}
