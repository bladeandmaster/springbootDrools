package com.pingan.core;

import com.pingan.core.Dto.RepayInfoDto;
import com.pingan.core.entity.AccountInfo;
import com.pingan.core.entity.MonthRepayInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class A0002Test {

    @Test
    public void test(){
        //kmodule.xml放在meta-inf下，drools能自动找到,
        // 但是spring-drools.xml需要通过@ImportResource("classpath:spring-drools.xml")找到
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        KieSession kieSession = kContainer.newKieSession("ksession-rules");

        RepayInfoDto repayInfoDto = new RepayInfoDto();
        AccountInfo accountInfo = new AccountInfo("888888","1","D1");
        MonthRepayInfo monthRepayInfo = new MonthRepayInfo("1","1","201903");
        repayInfoDto.setAccountInfo(accountInfo);
        repayInfoDto.setMonthRepayInfo(monthRepayInfo);

        RepayInfoDto repayInfoDto2 = new RepayInfoDto();
        AccountInfo accountInfo2 = new AccountInfo("999999","1","D1");
        MonthRepayInfo monthRepayInfo2 = new MonthRepayInfo("2","1","201903");
        repayInfoDto2.setAccountInfo(accountInfo2);
        repayInfoDto2.setMonthRepayInfo(monthRepayInfo2);

        List<RepayInfoDto> repayInfoList = new ArrayList<>();
        repayInfoList.add(repayInfoDto);
        repayInfoList.add(repayInfoDto2);

        kieSession.insert(repayInfoList);
        boolean flag = true;
        kieSession.insert(flag);
        //kieSession.setGlobal("log",log);
        int ruleFiredCount = kieSession.fireAllRules();
        kieSession.dispose();
        //log.info("触发了{}条规则",ruleFiredCount);
    }
}


