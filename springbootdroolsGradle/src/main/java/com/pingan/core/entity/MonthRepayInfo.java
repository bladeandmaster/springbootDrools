package com.pingan.core.entity;

import java.util.List;

//每个月还款信息
public class MonthRepayInfo {
    String sortNo;
    String repaySatus;
    String monthDate;

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    List<Customer> customerList;

    public MonthRepayInfo() {
    }

    public MonthRepayInfo(String sortNo, String repaySatus, String monthDate) {
        this.sortNo = sortNo;
        this.repaySatus = repaySatus;
        this.monthDate = monthDate;
    }

    public String getSortNo() {
        return sortNo;
    }

    public void setSortNo(String sortNo) {
        this.sortNo = sortNo;
    }

    public String getRepaySatus() {
        return repaySatus;
    }

    public void setRepaySatus(String repaySatus) {
        this.repaySatus = repaySatus;
    }

    public String getMonthDate() {
        return monthDate;
    }

    public void setMonthDate(String monthDate) {
        this.monthDate = monthDate;
    }

    @Override
    public String toString() {
        return "MonthRepayInfo{" +
                "sortNo='" + sortNo + '\'' +
                ", repaySatus='" + repaySatus + '\'' +
                ", monthDate='" + monthDate + '\'' +
                '}';
    }
}
