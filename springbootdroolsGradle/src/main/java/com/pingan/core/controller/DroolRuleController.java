package com.pingan.core.controller;

import com.pingan.core.entity.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.cdi.KBase;
import org.kie.api.cdi.KReleaseId;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class DroolRuleController {

//new ClassPathResource(location.substring(CLASSPATH_URL_PREFIX.length()), getClassLoader());
//使用KModuleBeanFactoryPostProcessor只能使用@Autowired方式
//使用KModuleAnnotationPostProcessor可以使用@Autowired和@KBase("kbase")两种方式

    @Autowired
    //@KBase("kbase")
    //@KReleaseId(groupId = "com.pingan.core",artifactId = "drools",version = "LATEST")
    private KieBase kieBase;

    //http://localhost:8080/rule1
    @RequestMapping("/rule1")
    @ResponseBody
    public String rule1(){
        //此方式需要在springboot启动类App里通过
        //@ImportResource("classpath:spring-drools.xml")找到drools与spring整合的配置文件

        //StatefulKnowledgeSession
        KieSession kieSession = kieBase.newKieSession();

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("superbing");
        userInfo.setTelephone("13618607409");

        kieSession.insert(userInfo);
        //kieSession.setGlobal("log",log);
        int firedCount = kieSession.fireAllRules();
        //kieSession.dispose();
        System.out.println("触发了" + firedCount + "条规则");
        return "触发了" + firedCount + "条规则";
    }


    @RequestMapping("/rule2")
    @ResponseBody
    public String rule3(){
        //这种方式需要在resource目录新建META-INF/kmoudule.xml文件
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        KieSession kieSession = kContainer.newKieSession("ksession-rules");

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("superbing");
        userInfo.setTelephone("13618607409");

        kieSession.insert(userInfo);
        //kieSession.setGlobal("log",log);
        int firedCount = kieSession.fireAllRules();
        //kieSession.dispose();
        System.out.println("触发了" + firedCount + "条规则");
        return "触发了" + firedCount + "条规则";
    }
}
