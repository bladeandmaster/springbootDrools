package com.pingan.core;

import com.pingan.core.entity.UserInfo;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.cdi.KBase;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@Slf4j
public class UserDroolTest {

    @Autowired
    //@KBase("kbase")
    //@KReleaseId(groupId = "com.pingan.core",artifactId = "drools",version = "LATEST")
    private KieBase kieBase;

    @Test
    public void rule1(){
        //StatefulKnowledgeSession
        KieSession kieSession = kieBase.newKieSession();

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("superbing");
        userInfo.setTelephone("13618607409");

        kieSession.insert(userInfo);
        //kieSession.setGlobal("log",log);
        int firedCount = kieSession.fireAllRules();
        //kieSession.dispose();
        System.out.println("触发了" + firedCount + "条规则");
    }


    @Test
    public void rule2(){
        //这种方式需要在resource目录新建META-INF/kmoudule.xml文件
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        KieSession kieSession = kContainer.newKieSession("ksession-rules");

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("superbing");
        userInfo.setTelephone("13618607409");

        kieSession.insert(userInfo);
        //kieSession.setGlobal("log",log);
        int firedCount = kieSession.fireAllRules();
        //kieSession.dispose();
        System.out.println("触发了" + firedCount + "条规则");
    }
    
    @Test
    public void showURL() throws IOException {

        // 第一种：获取类加载的根路径
        // idea 测试环境下  D:\java_work\springbootdrools\out\test\classes
    	//eclipse D:\java_work\springbootdrools\bin\main
        File f = new File(this.getClass().getResource("/").getPath());
        System.out.println(f);

        //如果不加“/”  获取当前类的加载目录  idea D:\java_work\springbootdrools\out\test\classes\com\pingan\core
        //eclipse D:\java_work\springbootdrools\bin\main\com\pingan\core
        File f2 = new File(this.getClass().getResource("").getPath());
        System.out.println(f2);

        // 第二种：获取项目路径    D:\java_work\springbootdrools
        //eclipse D:\java_work\springbootdrools
        File directory = new File("");// 参数为空
        String courseFile = directory.getCanonicalPath();
        System.out.println(courseFile);


        // 第三种：idea  file:/D:/java_work/springbootdrools/out/test/classes/
        //eclipse file:/D:/java_work/springbootdrools/bin/main/
        URL xmlpath = this.getClass().getClassLoader().getResource("");
        System.out.println(xmlpath);


        // 第四种： idea D:\java_work\springbootdrools
        //eclipse D:\java_work\springbootdrools
        System.out.println(System.getProperty("user.dir"));


        // 第五种： 获取所有的类路径 包括jar包的路径
        System.out.println(System.getProperty("java.class.path"));

    }    
}
