package com.pingan.core;

import com.pingan.core.Dto.PbcReportDto;
import com.pingan.core.entity.AccountInfo;
import com.pingan.core.entity.Customer;
import com.pingan.core.entity.MonthRepayInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


//@RunWith(SpringRunner.class)
//@SpringBootTest
public class A0001Test {

    @Test
    public void test(){
        //kmodule.xml放在meta-inf下，drools能自动找到,
        // 但是spring-drools.xml需要通过@ImportResource("classpath:spring-drools.xml")找到
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        KieSession kieSession = kContainer.newKieSession("ksession-rules");

        PbcReportDto reportDto = new PbcReportDto();
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setCreditCardNmuber("888888");
        accountInfo.setAccountStatus("1");
        accountInfo.setAccountType("D1");

        List<MonthRepayInfo> latest24MonthRepayInfo = new ArrayList<>();
        MonthRepayInfo monthRepayInfo1 = new MonthRepayInfo("1","1","201903");
        MonthRepayInfo monthRepayInfo2 = new MonthRepayInfo("2","2","201902");

        List<Customer> customerList1 = new ArrayList<>();
        customerList1.add(new Customer("111","张一"));
        customerList1.add(new Customer("222","张二"));
        monthRepayInfo1.setCustomerList(customerList1);
        latest24MonthRepayInfo.add(monthRepayInfo1);

        List<Customer> customerList2 = new ArrayList<>();
        customerList2.add(new Customer("111","李一"));

        monthRepayInfo2.setCustomerList(customerList2);
        latest24MonthRepayInfo.add(monthRepayInfo2);

        reportDto.setAccountInfo(accountInfo);
        reportDto.setLatest24MonthRepayInfo(latest24MonthRepayInfo);

        kieSession.insert(reportDto);
        int ruleFiredCount = kieSession.fireAllRules();
        kieSession.dispose();
        System.out.println("触发了" + ruleFiredCount + "条规则");
    }
}


