package com.pingan.core.entity;

/*
 *@desc
 *@author qhb871
 *@date 2019/3/29 21:44
 */
public class AccountInfo {
    private String creditCardNmuber;
    private String accountType;
    private String accountStatus;

    public AccountInfo() {
    }

    public AccountInfo(String creditCardNmuber, String accountType, String accountStatus) {
        this.creditCardNmuber = creditCardNmuber;
        this.accountType = accountType;
        this.accountStatus = accountStatus;
    }

    public String getCreditCardNmuber() {
        return creditCardNmuber;
    }

    public void setCreditCardNmuber(String creditCardNmuber) {
        this.creditCardNmuber = creditCardNmuber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "creditCardNmuber='" + creditCardNmuber + '\'' +
                ", accountType='" + accountType + '\'' +
                ", accountStatus='" + accountStatus + '\'' +
                '}';
    }
}
