package com.pingan.core.controller;

import com.pingan.core.entity.UserInfo;
import com.pingan.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*
 *@desc
 *@author qhb871
 *@date 2018/12/28 22:55
*/
@Controller
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    //测试接口 http://localhost:8080/user/index
    @RequestMapping(value = "/index")
    public String index(){
        return "user/index";
    }

    //测试接口 http://localhost:8080/user/show?name=xiaoming
    @RequestMapping(value = "/show")
    @ResponseBody
    public String show(@RequestParam(value = "name")String name){
        return "收到了"+name;
    }

    //测试接口 http://localhost:8080/user/userList?name=xiaoming
    @RequestMapping(value = "/userList")
    @ResponseBody
    public List<UserInfo> userList(@RequestParam(value = "name")String name){
        List<UserInfo> userInfoList = userService.selectAllUsers();
        return userInfoList;
    }
}
