package com.pingan.core.dao;

import com.pingan.core.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserDao {

    List<UserInfo> selectAllUsers();
}
