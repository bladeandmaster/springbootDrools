package com.pingan.core.Dto;

import com.pingan.core.entity.AccountInfo;
import com.pingan.core.entity.MonthRepayInfo;


public class RepayInfoDto {

    private AccountInfo accountInfo;
    private MonthRepayInfo monthRepayInfo;

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public MonthRepayInfo getMonthRepayInfo() {
        return monthRepayInfo;
    }

    public void setMonthRepayInfo(MonthRepayInfo monthRepayInfo) {
        this.monthRepayInfo = monthRepayInfo;
    }
}
