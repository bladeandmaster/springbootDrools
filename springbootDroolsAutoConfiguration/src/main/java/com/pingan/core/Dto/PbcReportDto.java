package com.pingan.core.Dto;

import com.pingan.core.entity.AccountInfo;
import com.pingan.core.entity.MonthRepayInfo;

import java.util.List;

//人行征信报告
public class PbcReportDto {
    private AccountInfo accountInfo;
    private List<MonthRepayInfo> latest24MonthRepayInfo;

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public List<MonthRepayInfo> getLatest24MonthRepayInfo() {
        return latest24MonthRepayInfo;
    }

    public void setLatest24MonthRepayInfo(List<MonthRepayInfo> latest24MonthRepayInfo) {
        this.latest24MonthRepayInfo = latest24MonthRepayInfo;
    }
}
