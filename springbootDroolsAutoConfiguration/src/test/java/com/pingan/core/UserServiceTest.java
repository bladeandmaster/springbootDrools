package com.pingan.core;

import com.pingan.core.App;
import com.pingan.core.entity.UserInfo;
import com.pingan.core.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

//SpringBoot1.4版本之前用的是SpringJUnit4ClassRunner.class
@RunWith(SpringRunner.class)
//SpringBoot1.4版本之前用的是@SpringApplicationConfiguration(classes = Application.class)
@SpringBootTest(classes = App.class)
public class UserServiceTest {
    @Autowired
    UserService userService;
    @Test
    public void selectAllUsersTest(){
        List<UserInfo> allUsers = userService.selectAllUsers();
        System.out.print(allUsers);
    }
}
